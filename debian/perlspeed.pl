#!/usr/bin/perl

eval 'use Tree::RedBlack';
if (0) {
    #use linear routines
    $val = sub {    # gets the number, returns name string
        return $names[ $_[0] ];
    };
    $locate =
      sub {    # gets an md5sum, returns the number in storage array (or undef)

        for ( 0 .. $#sums ) {
            if ( $sums[$_] eq $_[0] ) { return $names[$_]; }
        }
        return undef;
    };
    $feed = sub {    # inserts an entry with sum=first-arg and name=second-arg
        push ( @sums,  $_[0] );
        push ( @names, $_[1] );
    };
}

if (1) {
    print STDERR "libtree-redblack-perl found, using tree structures.\n";
    $t   = new Tree::RedBlack;
    $val =
      sub { return $_[0]->val }; # gets the node reference, returns value (name string)
    $locate =
      sub { return $t->node( $_[0] ) };    # gets the md5sum, returns a node reference (or undef)
    $feed = sub {
        return $t->insert( $_[0], $_[1] );
      }    # inserts a node with key=first-arg and name=second-arg
}

if (0) {
    print STDERR "using hash.\n";
    $val =
      sub { return $_[0]->val }; # gets the node reference, returns value (name string)
    $locate =
      sub { return $HASH{ $_[0] } };    # gets the md5sum, returns a node reference (or undef)
    $feed = sub { $HASH{ $_[0] } = $_[1] };
}

open( find, "locate . |" );

FILE: while (<find>) {

    print $i++,"\n";
    if ( !defined( $probe = &$locate($sum) ) )
    {    # look for the existing node in db $thefile

        #      &$feed($sum, $thefile); # file is new, feeding database
        &$feed( $_, $_ );

        #      print "NEU: Summe: $sum, Datei: $thefile\n";
    }
}
